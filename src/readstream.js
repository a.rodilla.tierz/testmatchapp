const fs = require("fs");
let stream = undefined;
const readWork = () => {
  while ((chunk=streamWork.read()) != null) {
    // console.error('chunk: ', chunk);
  }
  if (streamWork.read() === null) {
    streamWork.destroy();
  }
};

const readerFile = async (textFileUri) => {
  console.error(textFileUri);
  streamWork = fs.createReadStream(textFileUri, {encoding: 'utf8'});
  return new Promise((resolve, reject) => {
    streamWork.on('readable', readWork);
    streamWork.on('error', (error) => {
      console.error('something was worng');
      console.error('error: ', error);
      resolve(error);
    });
    streamWork.on('close', (trouble) => {
      console.error('was finnish');
      resolve('success');
    });
  });
};

module.exports = readerFile;
