const mySQL = require('mysql');

const QueryMySQL = class {
  constructor() {
    this.connection = mySQL.createConnection({
      host: process.env['MYSQL_HOST'],
      user: process.env['MYSQL_USER'],
      password: process.env['MYSQL_PASSWORD'],
      database: process.env['MYSQL_DATABASE']
    });
    this.connection.connect();
  }

  async getIdWork() {
    return new Promise((resolve, reject) => {
      this.connection.query("SELECT clientId FROM clients WHERE clientName='name';", (error, resp, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(resp);
        }
      });
    });
  };

  async getTransactionsWork(id) {
    return new Promise((resolve, reject) => {
      this.connection.query("SELECT * FROM transactions WHERE clientId=" + id, (error, resp, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(resp);
        }
      });
    });
  };

  async getUpdateTransactions(transac) {
    return new Promise((resolve, reject) => {
      this.connection.query("UPDATE transactions SET value = " + (transac.value*0.1) + " WHERE id=" + transac.id, (error, resp, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(resp);
        }
      });
    });
  };

  async getUpdateInverseTransactions(transac) {
    return new Promise((resolve, reject) => {
      this.connection.query("UPDATE transactions SET value = " + (transac.value*10) + " WHERE id=" + transac.id, (error, resp, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(resp);
        }
      });
    });
  };

  async testHellCAllback() {
    const process = {
      success: 0,
      fail: 0
    };

    const id = await this.getIdWork();
    const transactions = await this.getTransactionsWork(id[0]['clientId']);
    for(const nodeTrans of transactions) {
      const respUpdate = await this.getUpdateTransactions(nodeTrans);
      if (respUpdate.serverStatus === 2) {
        process.success += 1;
      } else {
        process.fail += 1;
      }
    };
    return process;
  }

  async testReverseHellCAllback() {
    const process = {
      success: 0,
      fail: 0
    };
    const id = await this.getIdWork();
    const transactions = await this.getTransactionsWork(id[0]['clientId']);
    for(const nodeTrans of transactions) {
      const tespUpdate = await this.getUpdateInverseTransactions(nodeTrans);
      if (tespUpdate.serverStatus === 2) {
        process.success += 1;
      } else {
        process.fail += 1;
      }
    };
    return process;
  }
}

module.exports = QueryMySQL;
