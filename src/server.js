const express = require('express');
const http = require('http');
const dotenv = require('dotenv');
const mongopool = require('./mongopool.js');
const hostname = '127.0.0.2';
const port = 3000;

const app = express();
const httpClient = require('request-promise-native');
app.set('httpClient', httpClient);
dotenv.config({path: '.env'});
app.set('clientMongo', new mongopool());
const server = app.listen(port, () => {
  console.log(
    '  App is running at http://localhost:%d in %s mode'
  );
  console.log('  Press CTRL-C to stop\n');
});
