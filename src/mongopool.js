const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";

const MongoManager = class {
  constructor() {
    this.workDBO = undefined;
    this.clientMongo = new MongoClient(url);
    this.getCollectionConect().then(collection => {
      console.error('collection in constructor');
      this.workDBO = collection;
    });
  }

  async conectorMongo() {
    this.workDBO = await this.getCollectionConect();
    await this.creatorCollection('matchApp');
  }

  async getCollectionConect() {
    return new Promise((resolve, reject) => {
      this.clientMongo.connect(function(err, db) {
        console.error('init mongodb');
        if (err) {
          console.error('infoError: ', err);
          reject(err);
        } else {
          resolve(db.db("mydb"));
        }
      });
    });
  }

  async creatorCollection(collectionName) {
    await this.workDBO.createCollection(collectionName, function(err, res) {
      if (err) throw err;
      console.log("Collection created!");
    });
  }

  async insertInCollection(collection, workObj)  {
    if (!this.workDBO) {
      await this.conectorMongo();
    }
    return new Promise((resolve, reject) => {
      this.workDBO.collection(collection).insertOne(workObj, function(err, res) {
        if (err) reject(err);
        resolve(res);
      });
    });
  }

  async queryToCollection(collection, query)  {
    if (!this.workDBO) {
      await this.conectorMongo();
    }
    return new Promise((resolve, reject) => {
      this.workDBO.collection(collection).find(query).toArray(function(err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }
}

module.exports = MongoManager;
