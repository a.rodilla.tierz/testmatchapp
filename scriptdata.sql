CREATE TABLE `clients` (
   `clientId` VARCHAR(255) NOT NULL PRIMARY KEY,
 `clientName` varchar(255) NOT NULL
);

insert into clients (`clientId`, `clientName`)
VALUES ('001', 'name');

    CREATE TABLE `transactions` (
             `id` INT(64) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       `clientId` VARCHAR(255) NOT NULL,
          `value` DECIMAL(8, 2) NOT NULL
    );

insert into transactions (`clientId`, `value`)
VALUES ('001', 10.0);
insert into transactions (`clientId`, `value`)
VALUES ('001', 12.0);
insert into transactions (`clientId`, `value`)
VALUES ('001', 15.9);
insert into transactions (`clientId`, `value`)
VALUES ('001', 8.0);
insert into transactions (`clientId`, `value`)
VALUES ('001', 20.0);
