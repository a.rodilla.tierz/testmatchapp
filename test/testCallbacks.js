
const hellCallBackManager = require('../src/hellCallBackManager.js');
const dotenv = require('dotenv');

test('the update works, happy path', async () => {
  dotenv.config({path: '.env'});
  const workerMySQL = new hellCallBackManager();
  const idResp = await workerMySQL.testHellCAllback();
  expect(idResp.success).toBe(5);
  expect(idResp.fail).toBe(0);
});

test('the update works reverset, happy path', async () => {
  dotenv.config({path: '.env'});
  const workerMySQL = new hellCallBackManager();
  const idResp = await workerMySQL.testReverseHellCAllback();
  expect(idResp.success).toBe(5);
  expect(idResp.fail).toBe(0);
});
