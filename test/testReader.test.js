const readerStream = require('../src/readstream.js');

test('the read in stream, happy path', async () => {
  const testReader = await readerStream('./textSample.txt');
  expect(testReader).toBe('success');
});

test('the read in stream, with error', async () => {
  const testReader = await readerStream('./textSample');
  expect(testReader).not.toBe('success');
});
