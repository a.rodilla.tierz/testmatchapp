const mongopool = require('../src/mongopool.js');

test('the manage db Mongo, for query', async () => {
  const clientMongo = new mongopool();
  const workQuery = {
    id: 234234234
  };
  const queryResponse = await clientMongo.queryToCollection('matchApp', workQuery);
  expect(queryResponse[0]['id']).toBe(workQuery.id);
});

test('the manage db Mongo, for insert', async () => {
  const clientMongo = new mongopool();
  const workObjInsert = {
    id: 236573,
    name: 'something',
    description: 'aiag iai asif gakufyg akufgy as'
  };
  console.error('will QUERY Datas');
  const insertResponse = await clientMongo.insertInCollection('matchApp', workObjInsert);
  expect(insertResponse.result.n).toBe(1);
  expect(insertResponse.result.ok).toBe(1);

  const workQuery = {
    id: 236573
  };
  const queryResponse = await clientMongo.queryToCollection('matchApp', workQuery);
  expect(queryResponse[0]['id']).toBe(workQuery.id);
});
